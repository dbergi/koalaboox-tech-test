# Challenge_

Koalaboox Code Challenge

Koalaboox Todo list

## Implement Todo list:

- [x] Add/Edit/Delete item

- [x] Mark done item

- [x] Reorder items

- [x] Show creation date, update date, done date

## Setup

You can use ```make``` to spin up project from start to run testing or rebuilding the entire project.

The main commands are:

- To re/build project
 ```bash
$ make start
```
!!!Note: The development client UI may take a while in being accessible. You can check ionic container to see when livereload is ready.

- To up docker-compose project
```bash
$ make up
```

- To stop containers
```bash
$ make stop
```

- To run testing
```bash 
$ make phpunit
```

To see all possible commands look in makefile
## Workflow:

When running up project client built project will be available in http://localhost:8081, access it to find the UI.

The development server will be in http://localhost:8100, there livereloading allows to see changes to client project inmediately.

!!!Note: The development client UI may take a while in being accessible. You can check ionic container log to see when livereload is ready.

The api server lives in http://localhost:8080. There you can query the swagger documentation of the API by going to http://localhost:8080/api/doc

#### Responses

The most part of responses of the API return back empty response.
This means successful request. You can check the return Status code too.

## Testing run

You can run testing with the command:

```bash 
$ make phpunit
```

## Architecture

![Architecture](docs/architecture.png)



