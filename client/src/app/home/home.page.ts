import { Component } from '@angular/core';
import {TodoListService} from '../services/todo-list.service';
import {ModalController} from '@ionic/angular';
import {NewTodoItemComponent} from '../components/new-todo-item/new-todo-item.component';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  public todoList = [];

  constructor(
      private todoListService: TodoListService,
      private modal: ModalController,
  ) {}

  ionViewWillEnter() {
    this.getTodoList();
  }

  async getTodoList() {
    const list = await this.todoListService.getTodoList();
    this.todoList = list.sort((a: any, b: any) => {
      return a.attributes.order - b.attributes.order;
    });
  }

  async addTodoItem() {
    const data = await this.showNewItemModal();

    if (data.data) {
      await this.todoListService.postTodoItem(data.data, this.todoList.length + 1);
      this.getTodoList();
    }
  }

  async showNewItemModal(text = null) {
    const modal = await this.modal.create({
      component: NewTodoItemComponent,
      componentProps: {
        text
      },
      backdropDismiss: false,
      cssClass: 'modal'
    });

    await modal.present();
    return await modal.onDidDismiss();
  }

  async editItem(item) {
    const data = await this.showNewItemModal(item.attributes.subject);

    if (data.data && data.data !== item.attributes.subject) {
      await this.todoListService.editTodoItem(item.id, data.data, item.attributes.order);
      this.getTodoList();
    }
  }

  async deleteItem(item) {
    await this.todoListService.deleteItem(item.id);
    await this.reorderItems(item.attributes.order);
    this.getTodoList();
  }

  async markDone(id) {
    await this.todoListService.markDone(id);
    this.getTodoList();
  }

  async reorderItems(initialPosition) {
    for (let i = initialPosition; i < this.todoList.length; i++) {
      const item = this.todoList[i];
      await this.todoListService.editTodoItem(item.id, item.attributes.subject, i);
    }
  }

  doReorder(ev: any) {
    this.todoList = ev.detail.complete(this.todoList);
    this.reorderItems(0);
    console.log(this.todoList);
  }
}
