import { Component, OnInit } from '@angular/core';
import {ModalController} from '@ionic/angular';

@Component({
  selector: 'app-new-todo-item',
  templateUrl: './new-todo-item.component.html',
  styleUrls: ['./new-todo-item.component.scss'],
})
export class NewTodoItemComponent implements OnInit {

  public text;

  constructor(
    public modal: ModalController,
  ) { }

  ngOnInit() {}
}
