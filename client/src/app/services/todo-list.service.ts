import { Injectable } from '@angular/core';
import {ApiService} from './api.service';
import {HelpersService} from './helpers.service';

@Injectable({
  providedIn: 'root'
})
export class TodoListService {

  constructor(
      private api: ApiService,
      private helpers: HelpersService,
  ) { }

  async getTodoList() {
    const endpoint = '/todoList';

    return await this.api.get(endpoint)
      .then(data => data.data as [])
      .catch(err => {
        if (/Resource not found/.test(err)) {
          this.helpers.displayToast('Empty todo list');
        } else {
          throw err;
        }
        return [];
      });
  }

  async postTodoItem(subject, order) {
    const body = {
      data: {
        subject,
        order
      }
    };

    return await this.api.post('/todoItem', body)
      .then(() => {
        this.helpers.displayToast('Ok', 1000, 'success');
      });
  }

  async editTodoItem(id, subject, order) {
    const body = {
      data: {
        id,
        subject,
        order: parseInt(order, 10)
      }
    };

    const endpoint = '/todoItem';

    return await this.api.put(endpoint, body);
  }

  async deleteItem(id) {
    const body = {
      data: {
        id
      }
    };

    const endpoint = '/todoItem';

    return await this.api.delete(endpoint, body)
      .then(() => {
        this.helpers.displayToast('Item deleted.', 1000, 'success');
      });
  }

  async markDone(id) {
    const body = {
      data: {
        id
      }
    };

    return await this.api.post('/todoItem/done', body)
      .then(() => {
        this.helpers.displayToast('Done', 1000, 'success');
      });
  }
}
