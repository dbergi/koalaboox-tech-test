import {ErrorHandler, Injectable} from '@angular/core';
import {AlertController, LoadingController} from '@ionic/angular';
import {HelpersService} from './helpers.service';

@Injectable({
  providedIn: 'root'
})
export class ErrorHandlerService implements ErrorHandler {

  constructor(
      private loading: LoadingController,
      private helper: HelpersService,
      private alert: AlertController,
  ) {
  }

  async handleError(error) {
    console.error(error);
    // do something with the exception
    setTimeout(() => {
      this.helper.displayToast(error.message ?
          error.message.replace(/(Uncaught \(in promise\):\s|\n(Type)?Error(.|\n)+)/gi, '') :
          error.toString(), 4000);
    }, 0);

    while (!!(await this.loading.getTop())) {
      await this.loading.dismiss();
    }
    while (!!(await this.alert.getTop())) {
      await this.alert.dismiss();
    }
    return false;
  }
}