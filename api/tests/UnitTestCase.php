<?php


namespace App\Tests;


use App\Domain\Repository\TodoItemRepositoryInterface;
use PHPUnit\Framework\MockObject\MockObject;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class UnitTestCase extends KernelTestCase
{
    private ?MockObject $repository = null;

    /** @return TodoItemRepositoryInterface|MockObject */
    protected function repository(): MockObject
    {
        return $this->repository = $this->repository ?: $this->createMock(TodoItemRepositoryInterface::class);
    }
}