<?php

namespace App\Tests\Domain\Entity;

use App\Domain\Entity\TodoItem;
use App\Domain\ValueObject\TodoSubject;
use Ramsey\Uuid\Uuid;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class TodoItemTest extends KernelTestCase
{

    /** @test */
    public function itCreatesAnItem(): void
    {
        $id = Uuid::uuid4();
        $subject = TodoSubject::fromString('test');
        $order = 1;
        $item = TodoItem::create($id, $subject, $order);

        $this->assertInstanceOf(TodoItem::class, $item);
    }
}