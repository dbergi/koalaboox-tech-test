<?php


namespace App\Tests\Application\Command\Update;


use App\Application\Command\TodoItemCommandHandler;
use App\Tests\UnitTestCase;
use Exception;
use Liip\TestFixturesBundle\Test\FixturesTrait;

class MarkDoneTodoItemCommandHandlerTest extends UnitTestCase
{
    use FixturesTrait;

    private ?TodoItemCommandHandler $handler;

    protected function setUp()
    {
        parent::setUp();

        $this->handler = new TodoItemCommandHandler($this->repository());
    }

    /** @test
     * @throws Exception
     */
    public function itMarksDoneTodoItem()
    {
        $this->loadFixtures([
            'App\Infrastructure\Shared\Fixtures\TodoListFixtures'
        ]);

        $this->repository()
            ->expects($this->once())
            ->method('store');

        $command = new MarkDoneTodoItemCommand('11111111-1111-1111-1111-111111111111');
        $this->handler->handleMarkDoneTodoItemCommand($command);
    }
}