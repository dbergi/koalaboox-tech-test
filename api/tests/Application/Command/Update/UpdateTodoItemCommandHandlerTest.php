<?php


namespace App\Tests\Application\Command\Update;


use App\Application\Command\Delete\DeleteTodoItemCommand;
use App\Application\Command\TodoItemCommandHandler;
use App\Application\Command\Update\UpdateTodoItemCommand;
use App\Domain\Entity\TodoItem;
use App\Domain\ValueObject\TodoSubject;
use App\Tests\UnitTestCase;
use Liip\TestFixturesBundle\Test\FixturesTrait;
use Ramsey\Uuid\Uuid;

class UpdateTodoItemCommandHandlerTest extends UnitTestCase
{
    use FixturesTrait;

    private ?TodoItemCommandHandler $handler;

    protected function setUp()
    {
        parent::setUp();

        $this->handler = new TodoItemCommandHandler($this->repository());
    }

    /** @test
     * @throws \Exception
     */
    public function itEditsSubject()
    {
        $this->loadFixtures([
            'App\Infrastructure\Shared\Fixtures\TodoListFixtures'
        ]);

        $this->repository()
            ->expects($this->once())
            ->method('store');

        $command = new UpdateTodoItemCommand('11111111-1111-1111-1111-111111111111', 'test edited', 21);
        $this->handler->handleUpdateTodoItemCommand($command);
    }

    /** @test
     * @throws \Exception
     */
    public function itGetsExceptionWhenTryingToEditDeletedTodoItem()
    {
        $item = TodoItem::create(Uuid::fromString('11111111-1111-1111-1111-111111111111'), TodoSubject::fromString('test'), 1);
        $item->setDeleted(true);

        $this->repository()->method('get')
            ->willReturn($item);

        $this->expectException(\Exception::class);
        $this->expectExceptionCode(403);

        $command = new UpdateTodoItemCommand('11111111-1111-1111-1111-111111111111', 'test edited', 21);
        $this->handler->handleUpdateTodoItemCommand($command);
    }
}