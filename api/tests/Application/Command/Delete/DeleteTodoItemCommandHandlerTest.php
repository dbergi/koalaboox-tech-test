<?php

namespace App\Tests\Application\Command\Delete;

use App\Application\Command\Delete\DeleteTodoItemCommand;
use App\Application\Command\TodoItemCommandHandler;
use App\Tests\UnitTestCase;
use Liip\TestFixturesBundle\Test\FixturesTrait;

class DeleteTodoItemCommandHandlerTest extends UnitTestCase
{

    use FixturesTrait;

    private ?TodoItemCommandHandler $handler;

    protected function setUp()
    {
        parent::setUp();

        $this->handler = new TodoItemCommandHandler($this->repository());
    }

    /** @test */
    public function itDeletesTodoItem(): void
    {
        $this->loadFixtures([
            'App\Infrastructure\Shared\Fixtures\TodoListFixtures'
        ]);

        $this->repository()
            ->expects($this->once())
            ->method('store');

        $command = new DeleteTodoItemCommand('11111111-1111-1111-1111-111111111111');
        $this->handler->handleDeleteTodoItemCommand($command);
    }
}