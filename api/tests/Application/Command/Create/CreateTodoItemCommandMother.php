<?php


namespace App\Tests\Application\Command\Create;


use App\Application\Command\Create\CreateTodoItemCommand;
use App\Domain\ValueObject\TodoSubject;
use Faker\Factory;

class CreateTodoItemCommandMother
{
    public static function create(TodoSubject $subject, int $order): CreateTodoItemCommand
    {
        return new CreateTodoItemCommand($subject->__toString(), $order);
    }

    public static function random(): CreateTodoItemCommand
    {
        $faker = Factory::create();
        return self::create(TodoSubject::fromString($faker->realText()), $faker->randomNumber());
    }
}