<?php

namespace App\Tests\Application\Command\Create;

use App\Application\Command\TodoItemCommandHandler;
use App\Domain\Entity\TodoItem;
use App\Domain\Event\TodoItemCreated;
use App\Infrastructure\Repository\TodoItemStore;
use App\Tests\Application\ApplicationTestCase;
use App\Tests\Infrastructure\Shared\Event\EventCollectorListener;
use Broadway\Domain\DomainMessage;

class CreateTodoItemCommandHandlerTest extends ApplicationTestCase
{

    private ?TodoItemStore $repository;

    private TodoItemCommandHandler $handler;

    private ?EventCollectorListener $eventCollector;

    protected function setUp(): void
    {
        parent::setUp();

        $this->eventCollector = $this->service(EventCollectorListener::class);
        $this->repository = $this->service(TodoItemStore::class);
    }

    /** @test */
    public function itShouldCreateValidTodoItem()
    {
        $command = CreateTodoItemCommandMother::random();
        $this->handle($command);

        /** @var DomainMessage[] $events */
        $events = $this->eventCollector->popEvents();

        $this->assertCount(1, $events);

        /** @var TodoItemCreated $event */
        $event = $events[0]->getPayload();

        $this->assertInstanceOf(TodoItemCreated::class, $event);

        $item = $this->repository->get($event->id);

        $this->assertInstanceOf(TodoItem::class, $item);
        $this->assertEquals($command->subject(), $item->getSubject());
        $this->assertEquals($command->order(), $item->getOrder());
    }
}