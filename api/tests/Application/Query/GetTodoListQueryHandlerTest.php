<?php

namespace App\Tests\Application\Query;

use App\Application\Query\Collection;
use App\Application\Query\Item;
use App\Application\Query\TodoList\GetTodoListQuery;
use League\Tactician\CommandBus as QueryBus;
use Liip\TestFixturesBundle\Test\FixturesTrait;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class GetTodoListQueryHandlerTest extends KernelTestCase
{

    use FixturesTrait;

    /** @var QueryBus|null */
    private ?QueryBus $queryBus;

    public function setUp()
    {
        parent::setUp();

        static::bootKernel();
        $this->queryBus = self::$container->get('tactician.commandbus.query');
    }

    /** @test */
    public function itRetrievesTheTodoList()
    {
        $this->loadFixtures(array(
            'App\Infrastructure\Shared\Fixtures\TodoListFixtures'
        ));

        $query = new GetTodoListQuery();

        /** @var Collection $items */
        $items = $this->queryBus->handle($query);

        $this->assertCount(21, $items->data);
        $this->assertInstanceOf(Item::class, array_shift($items->data));
    }
}