<?php


namespace App\Tests\Infrastructure\UI\Http\Controller;


use Liip\TestFixturesBundle\Test\FixturesTrait;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class UpdateTodoItemControllerTest extends WebTestCase
{

    use FixturesTrait;

    /** @test */
    public function itGetsStatusCode200WhenCallingPUTTodoItemEndpoint(): void
    {
        $client = static::createClient();

        $this->loadFixtures([
            'App\Infrastructure\Shared\Fixtures\TodoListFixtures'
        ]);

        $client->request('PUT', '/api/todoItem', [], [], ['CONTENT_TYPE' => 'application/json'],
            json_encode(["data" => [
                "id" => "11111111-1111-1111-1111-111111111111",
                "subject" => "edit test",
                "order" => 2
            ]])
        );

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }
}