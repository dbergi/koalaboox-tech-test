<?php


namespace App\Tests\Infrastructure\UI\Http\Controller;


use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class CreateTodoItemControllerTest extends WebTestCase
{

    /** @test */
    public function itGetsStatusCode200WhenCallingPOSTTodoItemEndpoint(): void
    {
        $client = static::createClient();

        $client->request('POST', '/api/todoItem', [], [], ['CONTENT_TYPE' => 'application/json'],
            json_encode(["data" => [
                "subject" => "test",
                "order" => 1
            ]])
        );

        $this->assertEquals(201, $client->getResponse()->getStatusCode());
    }
}