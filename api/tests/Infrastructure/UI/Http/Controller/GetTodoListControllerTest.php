<?php

namespace App\Tests\Infrastructure\UI\Http\Controller;

use Liip\TestFixturesBundle\Test\FixturesTrait;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class GetTodoListControllerTest extends WebTestCase
{
    use FixturesTrait;

    /** @test */
    public function retrievingTodoListEndpointGetsStatusCode200()
    {
        $client = static::createClient();

        $this->loadFixtures([
            'App\Infrastructure\Shared\Fixtures\TodoListFixtures'
        ]);

        $client->request('GET', '/api/todoList');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

    /** @test */
    public function itGetsStatusCode404WhenGettingAnEmptyList(): void
    {
        $client = static::createClient();

        $this->loadFixtures([]);

        $client->request('GET', '/api/todoList');

        $this->assertEquals(404, $client->getResponse()->getStatusCode());
    }
}
