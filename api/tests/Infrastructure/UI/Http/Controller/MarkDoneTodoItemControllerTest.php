<?php


namespace App\Tests\Infrastructure\UI\Http\Controller;


use Liip\TestFixturesBundle\Test\FixturesTrait;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class MarkDoneTodoItemControllerTest extends WebTestCase
{
    use FixturesTrait;

    /** @test */
    public function itGetsStatusCode200WhenCallingPUTTodoItemDoneEndpoint(): void
    {
        $client = static::createClient();

        $this->loadFixtures([
            'App\Infrastructure\Shared\Fixtures\TodoListFixtures'
        ]);

        $client->request('POST', '/api/todoItem/done', [], [], ['CONTENT_TYPE' => 'application/json'],
            json_encode(["data" => [
                "id" => "11111111-1111-1111-1111-111111111111"
            ]])
        );

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }
}
