<?php

namespace App\Domain\Repository;

use App\Domain\Entity\TodoItem;
use Ramsey\Uuid\UuidInterface;

interface TodoItemRepositoryInterface
{
    public function get(UuidInterface $id): TodoItem;

    public function store(TodoItem $item): void;
}