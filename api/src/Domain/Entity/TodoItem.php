<?php

namespace App\Domain\Entity;

use App\Domain\Event\TodoItemCreated;
use App\Domain\Event\TodoItemDeleted;
use App\Domain\Event\TodoItemDone;
use App\Domain\Event\TodoItemUpdated;
use App\Domain\ValueObject\DateTime;
use App\Domain\ValueObject\TodoSubject;
use Broadway\EventSourcing\EventSourcedAggregateRoot;
use Ramsey\Uuid\UuidInterface;

class TodoItem extends EventSourcedAggregateRoot
{
    private UuidInterface $id;

    private TodoSubject $subject;

    private bool $done;

    private int $order;

    private bool $deleted;

    private DateTime $createdAt;

    private ?DateTime $updatedAt;

    private ?DateTime $doneAt;

    public static function create(UuidInterface $id, TodoSubject $subject, int $order)
    {
        $todoSubject = $subject;

        $item = new self();

        $item->apply(new TodoItemCreated($id, $todoSubject, $order, 0, 0, DateTime::now(), null, null));

        return $item;
    }

    public function getAggregateRootId(): string
    {
        return $this->id;
    }

    protected function applyTodoItemCreated(TodoItemCreated $event): void
    {
        $this->id = $event->id;
        $this->subject = $event->subject;
        $this->done = $event->done;
        $this->deleted = $event->deleted;
        $this->order = $event->order;
        $this->createdAt = $event->createdAt;
        $this->updatedAt = $event->updatedAt;
        $this->doneAt = $event->doneAt;
    }

    public function applyTodoItemUpdated(TodoItemUpdated $event): void
    {
        $this->setOrder($event->order);

        if ($this->subject->__toString() !== $event->subject->__toString()) {
            $this->setSubject($event->subject);
            $this->setUpdatedAt($event->updatedAt);
        }
    }

    public function applyTodoItemDeleted(TodoItemDeleted $event): void
    {
        $this->setDeleted($event->deleted);
        $this->setUpdatedAt($event->updatedAt);
    }

    public function applyTodoItemDone(TodoItemDone $event): void
    {
        $this->setDone($event->done);
        $this->setDoneAt($event->doneAt);
    }

    public function updateItem(string $subject, int $order): void
    {
        $this->apply(new TodoItemUpdated($this->id, TodoSubject::fromString($subject), $order, $this->done, $this->deleted, $this->createdAt, DateTime::now()));
    }

    public function deleteItem(): void
    {
        $this->apply(new TodoItemDeleted($this->id, $this->subject, $this->order, $this->done, 1, $this->createdAt, DateTime::now()));
    }

    public function markDone()
    {
        $this->apply(new TodoItemDone($this->id, $this->subject, $this->order, 1, $this->deleted, $this->createdAt, $this->updatedAt, DateTime::now()));
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id->toString();
    }

    /**
     * @param UuidInterface $id
     */
    public function setId(UuidInterface $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getSubject(): string
    {
        return $this->subject->__toString();
    }

    /**
     * @param TodoSubject $subject
     */
    public function setSubject(TodoSubject $subject): void
    {
        $this->subject = $subject;
    }

    /**
     * @return bool
     */
    public function isDone(): bool
    {
        return $this->done;
    }

    /**
     * @param bool $done
     */
    public function setDone(bool $done): void
    {
        $this->done = $done;
    }

    /**
     * @return bool
     */
    public function isDeleted(): bool
    {
        return $this->deleted;
    }

    /**
     * @param bool $deleted
     */
    public function setDeleted(bool $deleted): void
    {
        $this->deleted = $deleted;
    }

    /**
     * @return int
     */
    public function getOrder(): int
    {
        return $this->order;
    }

    /**
     * @param int $order
     */
    public function setOrder(int $order): void
    {
        $this->order = $order;
    }

    /**
     * @return string
     */
    public function getCreatedAt(): string
    {
        return $this->createdAt->toString();
    }

    /**
     * @param DateTime $createdAt
     */
    public function setCreatedAt(DateTime $createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return string
     */
    public function getUpdatedAt(): string
    {
        return $this->updatedAt->toString();
    }

    /**
     * @param DateTime $updatedAt
     */
    public function setUpdatedAt(DateTime $updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return string
     */
    public function getDoneAt(): string
    {
        return $this->doneAt->toString();
    }

    /**
     * @param DateTime $doneAt
     */
    public function setDoneAt(DateTime $doneAt): void
    {
        $this->doneAt = $doneAt;
    }

}
