<?php

namespace App\Domain\ValueObject;

class TodoSubject
{

    /** @var string */
    private string $subject;

    public static function fromString(string $subject): self
    {
        $todoSubject = new self();
        $todoSubject->subject = $subject;

        return $todoSubject;
    }

    public function __toString(): string
    {
        return $this->subject;
    }
}