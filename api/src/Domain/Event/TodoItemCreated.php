<?php


namespace App\Domain\Event;


use App\Domain\ValueObject\DateTime;
use App\Domain\ValueObject\TodoSubject;
use Assert\Assertion;
use Broadway\Serializer\Serializable;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

class TodoItemCreated implements Serializable
{

    public UuidInterface $id;

    public TodoSubject $subject;

    public int $order;

    public bool $done;

    public bool $deleted;

    public DateTime $createdAt;

    public ?DateTime $updatedAt;

    public ?DateTime $doneAt;

    /**
     * TodoItemAdded constructor.
     * @param UuidInterface $id
     * @param TodoSubject $todoSubject
     * @param int $order
     * @param bool $done
     * @param bool $deleted
     * @param DateTime $createdAt
     * @param DateTime|null $updatedAt
     * @param DateTime|null $doneAt
     */
    public function __construct(UuidInterface $id, TodoSubject $todoSubject, int $order, bool $done, bool $deleted, DateTime $createdAt, ?DateTime $updatedAt, ?DateTime $doneAt)
    {
        $this->id = $id;
        $this->subject = $todoSubject;
        $this->order = $order;
        $this->done = $done;
        $this->deleted = $deleted;
        $this->createdAt = $createdAt;
        $this->updatedAt = $updatedAt;
        $this->doneAt = $doneAt;
    }

    /**
     * @inheritDoc
     */
    public static function deserialize(array $data)
    {
        Assertion::keyExists($data, 'id');
        Assertion::keyExists($data, 'subject');
        Assertion::keyExists($data, 'order');
        Assertion::keyExists($data, 'done');
        Assertion::keyExists($data, 'deleted');
        Assertion::keyExists($data, 'created_at');
        Assertion::keyExists($data, 'updated_at');
        Assertion::keyExists($data, 'done_at');

        return new self(
            Uuid::fromString($data['id']),
            TodoSubject::fromString($data['subject']),
            $data['order'],
            $data['done'],
            $data['deleted'],
            DateTime::fromString($data['created_at']),
            isset($data['updated_at']) ? DateTime::fromString($data['updated_at']) : null,
            isset($data['done_at']) ? DateTime::fromString($data['done_at']) : null
        );
    }

    public function serialize(): array
    {
        return [
            'id' => $this->id->toString(),
            'subject' => $this->subject->__toString(),
            'order' => $this->order,
            'done' => $this->done,
            'deleted' => $this->deleted,
            'created_at' => $this->createdAt->toString(),
            'updated_at' => isset($this->updatedAt) ? $this->updatedAt->toString() : null,
            'done_at' => isset($this->doneAt) ? $this->doneAt->toString() : null,
        ];
    }
}