<?php

namespace App\Infrastructure\Shared\Fixtures;

use App\Domain\Entity\TodoItem;
use App\Domain\Repository\TodoItemRepositoryInterface;
use App\Domain\ValueObject\TodoSubject;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Ramsey\Uuid\Uuid;
use Faker\Factory;

class TodoListFixtures extends Fixture
{

    private TodoItemRepositoryInterface $repository;

    public function __construct(TodoItemRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    public function load(ObjectManager $manager)
    {
        $faker = Factory::create();

        for ($i = 0; $i < 20; $i++) {
            $item = TodoItem::create(Uuid::uuid4(), TodoSubject::fromString($faker->text()), $i + 1);
            $this->repository->store($item);
        }

        $item = TodoItem::create(Uuid::fromString('11111111-1111-1111-1111-111111111111'), TodoSubject::fromString('test'), 21);
        $this->repository->store($item);
    }
}