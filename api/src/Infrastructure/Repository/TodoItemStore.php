<?php

namespace App\Infrastructure\Repository;

use App\Domain\Entity\TodoItem;
use App\Domain\Repository\TodoItemRepositoryInterface;
use Broadway\EventHandling\EventBus;
use Broadway\EventSourcing\AggregateFactory\PublicConstructorAggregateFactory;
use Broadway\EventSourcing\EventSourcingRepository;
use Broadway\EventStore\EventStore;
use Ramsey\Uuid\UuidInterface;

class TodoItemStore extends EventSourcingRepository implements TodoItemRepositoryInterface
{
    public function __construct(
        EventStore $eventStore,
        EventBus $eventBus,
        array $eventStreamDecorators = []
    )
    {
        parent::__construct(
            $eventStore,
            $eventBus,
            TodoItem::class,
            new PublicConstructorAggregateFactory(),
            $eventStreamDecorators
        );
    }

    public function get(UuidInterface $id): TodoItem
    {
        /** @var TodoItem $item */
        $item = $this->load($id->toString());

        return $item;
    }

    public function store(TodoItem $item): void
    {
        $this->save($item);
    }
}