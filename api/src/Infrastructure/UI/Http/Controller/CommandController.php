<?php

declare(strict_types=1);

namespace App\Infrastructure\UI\Http\Controller;


use Broadway\CommandHandling\CommandBus;

abstract class CommandController
{

    private CommandBus $commandBus;

    public function __construct(CommandBus $commandBus)
    {
        $this->commandBus = $commandBus;
    }

    protected function exec($command): void
    {
        $this->commandBus->dispatch($command);
    }
}
