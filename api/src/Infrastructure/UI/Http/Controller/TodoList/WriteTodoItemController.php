<?php


namespace App\Infrastructure\UI\Http\Controller\TodoList;


use App\Application\Command\Create\CreateTodoItemCommand;
use App\Application\Command\Delete\DeleteTodoItemCommand;
use App\Application\Command\Update\UpdateTodoItemCommand;
use App\Infrastructure\UI\Http\Controller\CommandController;
use App\Tests\Application\Command\Update\MarkDoneTodoItemCommand;
use Assert\Assertion;
use Assert\AssertionFailedException;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class WriteTodoItemController extends CommandController
{

    /**
     * @Route (
     *     "/todoItem",
     *     name="create_todoItem",
     *     methods={"POST"}
     * )
     *
     * @SWG\Response(
     *     response=201,
     *     description="An empty string"
     * )
     *
     * @SWG\Response(
     *     response=400,
     *     description="Bad request",
     *     @SWG\Schema(type="object",
     *         @SWG\Property(property="errors", type="object",
     *             @SWG\Property(property="title", type="string"),
     *             @SWG\Property(property="detail", type="string"),
     *             @SWG\Property(property="code", type="integer"),
     *             @SWG\Property(property="status", type="integer")
     *         )
     *     )
     * )
     *
     * @SWG\Response(
     *     response=404,
     *     description="Not found",
     *     @SWG\Schema(type="object",
     *         @SWG\Property(property="errors", type="object",
     *             @SWG\Property(property="title", type="string"),
     *             @SWG\Property(property="detail", type="string"),
     *             @SWG\Property(property="code", type="integer"),
     *             @SWG\Property(property="status", type="integer")
     *         )
     *     )
     * )
     *
     * @SWG\Parameter(
     *     name="body",
     *     type="json",
     *     in="body",
     *     schema=@SWG\Schema(type="object",
     *         @SWG\Property(property="data", type="object", properties={
     *             @SWG\Property(property="subject", type="string"),
     *             @SWG\Property(property="order", type="integer"),
     *         })
     *     )
     * )
     *
     *  @SWG\Tag(name="TodoItem")
     *
     * @param Request $request
     * @return JsonResponse
     * @throws AssertionFailedException
     */
    public function createTodoItem(Request $request): JsonResponse
    {
        $data = $request->get('data');

        Assertion::string($data['subject'], "'subject' is not valid string");
        Assertion::integer($data['order'], "'order' is not valid integer");

        $command = new CreateTodoItemCommand($data['subject'], $data['order']);

        $this->exec($command);

        return new JsonResponse(null, 201);
    }

    /**
     * @Route (
     *     "/todoItem",
     *     name="update_todoItem",
     *     methods={"PUT"}
     * )
     *
     * @SWG\Response(
     *     response=200,
     *     description="An empty string"
     * )
     *
     * @SWG\Response(
     *     response=400,
     *     description="Bad request",
     *     @SWG\Schema(type="object",
     *         @SWG\Property(property="errors", type="object",
     *             @SWG\Property(property="title", type="string"),
     *             @SWG\Property(property="detail", type="string"),
     *             @SWG\Property(property="code", type="integer"),
     *             @SWG\Property(property="status", type="integer")
     *         )
     *     )
     * )
     *
     * @SWG\Response(
     *     response=404,
     *     description="Not found",
     *     @SWG\Schema(type="object",
     *         @SWG\Property(property="errors", type="object",
     *             @SWG\Property(property="title", type="string"),
     *             @SWG\Property(property="detail", type="string"),
     *             @SWG\Property(property="code", type="integer"),
     *             @SWG\Property(property="status", type="integer")
     *         )
     *     )
     * )
     *
     * @SWG\Parameter(
     *     name="body",
     *     type="json",
     *     in="body",
     *     schema=@SWG\Schema(type="object",
     *         @SWG\Property(property="data", type="object", properties={
     *             @SWG\Property(property="id", type="string"),
     *             @SWG\Property(property="subject", type="string"),
     *             @SWG\Property(property="order", type="integer"),
     *         })
     *     )
     * )
     *
     *  @SWG\Tag(name="TodoItem")
     *
     * @param Request $request
     * @return JsonResponse
     * @throws AssertionFailedException
     */
    public function updateTodoItem(Request $request): JsonResponse
    {
        $data = $request->get('data');

        Assertion::uuid($data['id'], "'id' is not valid uuid");
        Assertion::string($data['subject'], "'subject' is not valid string");
        Assertion::integer($data['order'], "'order' is not valid integer");

        $command = new UpdateTodoItemCommand($data['id'], $data['subject'], $data['order']);

        $this->exec($command);

        return new JsonResponse(null, 200);
    }

    /**
     * @Route (
     *     "/todoItem/done",
     *     name="mark_done_todoItem",
     *     methods={"POST"}
     * )
     *
     * @SWG\Response(
     *     response=200,
     *     description="An empty string"
     * )
     *
     * @SWG\Response(
     *     response=400,
     *     description="Bad request",
     *     @SWG\Schema(type="object",
     *         @SWG\Property(property="errors", type="object",
     *             @SWG\Property(property="title", type="string"),
     *             @SWG\Property(property="detail", type="string"),
     *             @SWG\Property(property="code", type="integer"),
     *             @SWG\Property(property="status", type="integer")
     *         )
     *     )
     * )
     *
     * @SWG\Response(
     *     response=404,
     *     description="Not found",
     *     @SWG\Schema(type="object",
     *         @SWG\Property(property="errors", type="object",
     *             @SWG\Property(property="title", type="string"),
     *             @SWG\Property(property="detail", type="string"),
     *             @SWG\Property(property="code", type="integer"),
     *             @SWG\Property(property="status", type="integer")
     *         )
     *     )
     * )
     *
     * @SWG\Parameter(
     *     name="body",
     *     type="json",
     *     in="body",
     *     schema=@SWG\Schema(type="object",
     *         @SWG\Property(property="data", type="object", properties={
     *             @SWG\Property(property="id", type="string"),
     *         })
     *     )
     * )
     *
     *  @SWG\Tag(name="TodoItem")
     *
     * @param Request $request
     * @return JsonResponse
     * @throws AssertionFailedException
     */
    public function markDoneTodoItem(Request $request): JsonResponse
    {
        $data = $request->get('data');

        Assertion::uuid($data['id'], "'id' is not valid uuid");

        $command = new MarkDoneTodoItemCommand($data['id']);

        $this->exec($command);

        return new JsonResponse(null, 200);
    }

    /**
     * @Route (
     *     "/todoItem",
     *     name="delete_todoItem",
     *     methods={"DELETE"}
     * )
     *
     * @SWG\Response(
     *     response=200,
     *     description="An empty string"
     * )
     *
     * @SWG\Response(
     *     response=400,
     *     description="Bad request",
     *     @SWG\Schema(type="object",
     *         @SWG\Property(property="errors", type="object",
     *             @SWG\Property(property="title", type="string"),
     *             @SWG\Property(property="detail", type="string"),
     *             @SWG\Property(property="code", type="integer"),
     *             @SWG\Property(property="status", type="integer")
     *         )
     *     )
     * )
     *
     * @SWG\Response(
     *     response=404,
     *     description="Not found",
     *     @SWG\Schema(type="object",
     *         @SWG\Property(property="errors", type="object",
     *             @SWG\Property(property="title", type="string"),
     *             @SWG\Property(property="detail", type="string"),
     *             @SWG\Property(property="code", type="integer"),
     *             @SWG\Property(property="status", type="integer")
     *         )
     *     )
     * )
     *
     * @SWG\Parameter(
     *     name="body",
     *     type="json",
     *     in="body",
     *     schema=@SWG\Schema(type="object",
     *         @SWG\Property(property="data", type="object", properties={
     *             @SWG\Property(property="id", type="string"),
     *         })
     *     )
     * )
     *
     *  @SWG\Tag(name="TodoItem")
     *
     * @param Request $request
     * @return JsonResponse
     * @throws AssertionFailedException
     */
    public function deleteTodoItem(Request $request): JsonResponse
    {
        $data = $request->get('data');

        Assertion::uuid($data['id'], "'id' is not valid uuid");

        $command = new DeleteTodoItemCommand($data['id']);

        $this->exec($command);

        return new JsonResponse(null, 200);
    }
}
