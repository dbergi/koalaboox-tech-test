<?php

namespace App\Infrastructure\UI\Http\Controller\TodoList;

use App\Application\Query\TodoList\GetTodoListQuery;
use App\Infrastructure\UI\Http\Controller\QueryController;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class ReadTodoListController extends QueryController
{

    /**
     * @Route (
     *     "/todoList",
     *     name="todoList",
     *     methods={"GET"}
     * )
     *
     * @SWG\Response(
     *     response=200,
     *     description="An empty string"
     * )
     *
     * @SWG\Response(
     *     response=400,
     *     description="Bad request",
     *     @SWG\Schema(type="object",
     *         @SWG\Property(property="errors", type="object",
     *             @SWG\Property(property="title", type="string"),
     *             @SWG\Property(property="detail", type="string"),
     *             @SWG\Property(property="code", type="integer"),
     *             @SWG\Property(property="status", type="integer")
     *         )
     *     )
     * )
     *
     * @SWG\Response(
     *     response=404,
     *     description="Not found",
     *     @SWG\Schema(type="object",
     *         @SWG\Property(property="errors", type="object",
     *             @SWG\Property(property="title", type="string"),
     *             @SWG\Property(property="detail", type="string"),
     *             @SWG\Property(property="code", type="integer"),
     *             @SWG\Property(property="status", type="integer")
     *         )
     *     )
     * )
     *
     *  @SWG\Tag(name="TodoItem")
     *
     */
    public function addTodoItem(): JsonResponse
    {
        $query = new GetTodoListQuery();

        $todoList = $this->ask($query);

        return $this->jsonCollection($todoList);
    }
}