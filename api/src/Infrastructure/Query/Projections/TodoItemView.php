<?php

namespace App\Infrastructure\Query\Projections;

use App\Domain\Exception\DateTimeException;
use App\Domain\ValueObject\DateTime;
use App\Domain\ValueObject\TodoSubject;
use Broadway\ReadModel\SerializableReadModel;
use Broadway\Serializer\Serializable;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

class TodoItemView implements SerializableReadModel
{

    private UuidInterface $id;

    private TodoSubject $subject;

    private int $order;

    private bool $done;

    private bool $deleted;

    private DateTime $createdAt;

    private ?DateTime $updatedAt;

    private ?DateTime $doneAt;


    /**
     * @param Serializable $event
     * @return TodoItemView
     * @throws DateTimeException
     */
    public static function fromSerializable(Serializable $event): self
    {
        return self::deserialize($event->serialize());
    }

    /**
     * @param array $data
     * @return TodoItemView
     * @throws DateTimeException
     */
    public static function deserialize(array $data): self
    {
        $instance = new self();

        $instance->id = Uuid::fromString($data['id']);
        $instance->subject = TodoSubject::fromString($data['subject']);
        $instance->done = $data['done'];
        $instance->deleted = $data['deleted'];
        $instance->order = $data['order'];
        $instance->createdAt = DateTime::fromString($data['created_at']);
        $instance->updatedAt = isset($data['updated_at']) ? DateTime::fromString($data['updated_at']) : null;
        $instance->doneAt = isset($data['done_at']) ? DateTime::fromString($data['done_at']) : null;

        return $instance;
    }

    public function serialize(): array
    {
        return [
            'id' => $this->getId(),
            'subject' => $this->subject()->__toString(),
            'done' => !!$this->done(),
            'deleted' => !!$this->deleted(),
            'order' => $this->order(),
            'created_at' => $this->createdAt(),
            'updated_at' => $this->updatedAt(),
            'done_at' => $this->doneAt(),
        ];
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function subject(): TodoSubject
    {
        return $this->subject;
    }

    public function done(): string
    {
        return $this->done;
    }

    public function deleted(): string
    {
        return $this->deleted;
    }

    public function order(): string
    {
        return $this->order;
    }

    public function createdAt(): string
    {
        return $this->createdAt->toString();
    }

    public function updatedAt(): ?string
    {
        return $this->updatedAt ? $this->updatedAt->toString() : null;
    }

    public function doneAt(): ?string
    {
        return $this->doneAt ? $this->doneAt->toString() : null;
    }

    /**
     * @param TodoSubject $subject
     */
    public function setSubject(TodoSubject $subject): void
    {
        $this->subject = $subject;
    }

    /**
     * @param int $order
     */
    public function setOrder(int $order): void
    {
        $this->order = $order;
    }

    /**
     * @param bool $done
     */
    public function setDone(bool $done): void
    {
        $this->done = $done;
    }

    /**
     * @param bool $deleted
     */
    public function setDeleted(bool $deleted): void
    {
        $this->deleted = $deleted;
    }

    /**
     * @param DateTime $createdAt
     */
    public function setCreatedAt(DateTime $createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @param DateTime|null $updatedAt
     */
    public function setUpdatedAt(?DateTime $updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @param DateTime|null $doneAt
     */
    public function setDoneAt(?DateTime $doneAt): void
    {
        $this->doneAt = $doneAt;
    }
}