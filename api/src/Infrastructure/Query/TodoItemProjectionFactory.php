<?php


namespace App\Infrastructure\Query;


use App\Domain\Event\TodoItemCreated;
use App\Domain\Event\TodoItemDeleted;
use App\Domain\Event\TodoItemDone;
use App\Domain\Event\TodoItemUpdated;
use App\Infrastructure\Query\MySQL\MysqlTodoItemReadModelRepository;
use App\Infrastructure\Query\Projections\TodoItemView;
use Broadway\ReadModel\Projector;

class TodoItemProjectionFactory extends Projector
{

    private MysqlTodoItemReadModelRepository $repository;

    public function __construct(MysqlTodoItemReadModelRepository $repository)
    {
        $this->repository = $repository;
    }

    protected function applyTodoItemCreated(TodoItemCreated $event)
    {
        $todoItemView = TodoItemView::fromSerializable($event);

        $this->repository->add($todoItemView);
    }

    protected function applyTodoItemUpdated(TodoItemUpdated $event)
    {
        /** @var TodoItemView $itemView */
        $itemView = $this->repository->find($event->id->toString());

        $itemView->setOrder($event->order);

        if ($itemView->subject()->__toString() !== $event->subject->__toString()) {
            $itemView->setSubject($event->subject);
            $itemView->setUpdatedAt($event->updatedAt);
        }

        $this->repository->apply();
    }

    protected function applyTodoItemDeleted(TodoItemDeleted $event)
    {
        /** @var TodoItemView $itemView */
        $itemView = $this->repository->find($event->id->toString());

        $this->repository->delete($itemView);
    }

    protected function applyTodoItemDone(TodoItemDone $event)
    {
        /** @var TodoItemView $itemView */
        $itemView = $this->repository->find($event->id->toString());

        $itemView->setDone($event->done);
        $itemView->setDoneAt($event->doneAt);

        $this->repository->apply();
    }
}
