<?php

namespace App\Infrastructure\Query\MySQL;

use App\Domain\Repository\TodoItemRepositoryInterface;
use App\Infrastructure\Query\Projections\TodoItemView;
use App\Infrastructure\Shared\Query\Repository\MysqlRepository;
use Doctrine\Persistence\ManagerRegistry;

final class MysqlTodoItemReadModelRepository extends MysqlRepository
{

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TodoItemView::class);
    }

    public function add(TodoItemView $item): void
    {
        $this->register($item);
    }

    public function delete(TodoItemView $item): void
    {
        $this->remove($item);
    }
}