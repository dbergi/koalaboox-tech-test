<?php

namespace App\Application\Query\TodoList;

use App\Application\Query\Collection;
use App\Application\Query\Item;
use App\Application\Query\QueryHandlerInterface;
use App\Infrastructure\Query\MySQL\MysqlTodoItemReadModelRepository;

class GetTodoListQueryHandler implements QueryHandlerInterface
{

    private MysqlTodoItemReadModelRepository $repository;

    public function __construct(MysqlTodoItemReadModelRepository $repository)
    {
        $this->repository = $repository;
    }

    public function handleGetTodoListQuery(GetTodoListQuery $query): Collection
    {
        $todoItems = $this->repository->findAll();

        $callback = function ($itemView) {
            return new Item($itemView);
        };

        return new Collection(1, 0, count($todoItems), array_map($callback, $todoItems));
    }
}