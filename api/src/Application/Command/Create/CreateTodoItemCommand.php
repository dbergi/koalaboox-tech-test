<?php

namespace App\Application\Command\Create;

use Ramsey\Uuid\UuidInterface;

class CreateTodoItemCommand
{

    private string $subject;

    private int $order;

    public function __construct(string $subject, int $order)
    {
        $this->subject = $subject;
        $this->order = $order;
    }

    public function subject(): string
    {
        return $this->subject;
    }

    public function order(): int
    {
        return $this->order;
    }
}