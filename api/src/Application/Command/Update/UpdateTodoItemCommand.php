<?php

namespace App\Application\Command\Update;

class UpdateTodoItemCommand
{

    private string $id;

    private string $subject;

    private int $order;

    public function __construct(string $id, string $subject, int $order)
    {
        $this->id = $id;
        $this->subject = $subject;
        $this->order = $order;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getSubject(): string
    {
        return $this->subject;
    }

    /**
     * @return int
     */
    public function getOrder(): int
    {
        return $this->order;
    }

}