<?php

namespace App\Application\Command\Delete;

class DeleteTodoItemCommand
{
    private string $id;

    public function __construct(string $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }
}