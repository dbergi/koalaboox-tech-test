<?php

namespace App\Application\Command;

use App\Application\Command\Create\CreateTodoItemCommand;
use App\Application\Command\Delete\DeleteTodoItemCommand;
use App\Application\Command\Update\UpdateTodoItemCommand;
use App\Domain\Entity\TodoItem;
use App\Domain\Repository\TodoItemRepositoryInterface;
use App\Domain\ValueObject\TodoSubject;
use App\Tests\Application\Command\Update\MarkDoneTodoItemCommand;
use Broadway\CommandHandling\SimpleCommandHandler;
use Exception;
use Ramsey\Uuid\Uuid;

class TodoItemCommandHandler extends SimpleCommandHandler implements CommandHandlerInterface
{

    private TodoItemRepositoryInterface $repository;

    public function __construct(TodoItemRepositoryInterface $todoItemRepository)
    {
        $this->repository = $todoItemRepository;
    }

    public function handleCreateTodoItemCommand(CreateTodoItemCommand $command): void
    {
        $todoItem = TodoItem::create(Uuid::uuid4(), TodoSubject::fromString($command->subject()), $command->order());

        $this->repository->store($todoItem);
    }

    public function handleUpdateTodoItemCommand(UpdateTodoItemCommand $command): void
    {
        $item = $this->repository->get(Uuid::fromString($command->getId()));

        if ($item->isDeleted()) {
            throw new Exception("You can't edit that item (" . $command->getId() . "). Item is deleted.", 403);
        }

        $item->updateItem($command->getSubject(), $command->getOrder());

        $this->repository->store($item);
    }

    public function handleDeleteTodoItemCommand(DeleteTodoItemCommand $command): void
    {
        $item = $this->repository->get(Uuid::fromString($command->getId()));

        $item->deleteItem();

        $this->repository->store($item);
    }

    public function handleMarkDoneTodoItemCommand(MarkDoneTodoItemCommand $command): void
    {
        $item = $this->repository->get(Uuid::fromString($command->getId()));

        $item->markDone();

        $this->repository->store($item);
    }
}